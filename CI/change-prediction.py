#!/usr/bin/env python
import requests
import json
import os
#Reading deployment factors from deployment.json
with open('deployment.json') as json_file:
    data = json.load(json_file)
#prediction_input = data['AppID']+","+data['Deployment type category']+","+ data['Numbre of bug fixes'] +","+ data['Number of features'] +","+ data['Number of POC'] +","+ data['Number of config'] +","+ data['Manually tested'] +","+ data['UAT done'] +","+ data['Integration test done'] +","+ data['Number of downstreams'] +","+ data['Number of upstreams'] +","+ data['Hosting platform type category']
prediction_input='0,10,44,88,44,5,1,0,11,1,0,1,0,7,0,7,10,0,50,60,1,0,1,0,1,0'
approval = requests.post('https://1nrg3v7ah3.execute-api.us-east-1.amazonaws.com/prod', json={'data':prediction_input })
output = str(approval.text)
print(output)
f= open("result.json","w+")
f.write(output)