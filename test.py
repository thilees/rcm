#!/usr/bin/env python
import requests
import json
import os

#Reading deployment factors from deployment.json
with open('deployment.json') as json_file:
    data = json.load(json_file)

prediction_input = data['AppID']+","+data['Deployment type category']+","+ data['Numbre of bug fixes'] +","+ data['Number of features'] +","+ data['Number of POC'] +","+ data['Number of config'] +","+ data['Manually tested'] +","+ data['UAT done'] +","+ data['Integration test done'] +","+ data['Number of downstreams'] +","+ data['Number of upstreams'] +","+ data['Hosting platform type category']

approval = requests.post('https://8ccd5294o3.execute-api.us-east-1.amazonaws.com/poc/pyapi', json={'data':prediction_input })
output = str(approval.text)
print(output[15:18])

